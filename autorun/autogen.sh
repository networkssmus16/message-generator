#!/bin/bash

zero=0


#Variables to change ------>
file="smallGrid"
priority=60
topology=2    #star = 1, grid = 2, mesh = 3
numNode=10
numCol
# <-----------

settingsfile=$file"/"$file"Settings"
messagefile=$file"/"$file"Messages"

for ((i=1;i<=1000;i++)); do 
	python settingsGen.py $settingsfile$i $messagefile$i $topology $numNode $priority $numCol
	if [ $((i%100)) -eq "$zero" ]
		then echo $i
	fi
done

#if you want to only generate mutliple settings, delete $i from $imessagefiles$i and vice versa