import random
import sys
#creates neighbors for each node with the max number of relations being numNodes/2


def createNeighbors():
	ratio = numNode / 2		#ratio to determine MAX number of neighbors
	for x in nodeList:
		nodeNeighbors[x] = []
	for x in nodeList:
		for y in range(1, random.randint(1,ratio)):		#pick random number of neighbors between 1 and ratio
			curr = str(random.randint(1,numNode))
			if x != curr:							#if the random node is NOT the current node
				nodeNeighbors[x].append(curr)		#append to neighbor list
				nodeNeighbors[curr].append(x)
		if len(nodeNeighbors[x]) == 0 and x != nodeList[0]:
			nodeNeighbors[x].append(str(1))
			nodeNeighbors['1'].append(str(x))
		elif len(nodeNeighbors[x]) == 0 and x == nodeList[0]:
			nodeNeighbors[x].append(str(2))
			nodeNeighbors['2'].append(str(1))
	for x in nodeList:
		nodeNeighbors[x] = set(nodeNeighbors[x])	#make it a set to prevent duplicates


def createPacketMessage():		#unused method to create binary sensor data
	message = ""
	for i in range(0,10):		#standard arduino uses 10 bits for temp sensor
		message += str(random.randint(0,1))
	return str(message)

def createPacketDestinations(src):	#create the mulitple destinations for a packet
	dest = []
	for i in range(0, random.randint(1, 5)):	#random number of destinations
		x = random.randint(1, numNode)
		if x != src and x not in dest:		#if the dest is NOT source and not already included
			dest.append(x)				#append to list
	if len(dest) == 0:
		dest.append(1)		#make 1 the destinations if no dests are created
	return dest

def createPacket(priorityRatio, src):	#src, [destinations], tick, priority
	packet = []
	packet.append(src)		#append the source
	packet.append(createPacketDestinations(src))	#create and append the destinations
	tick = random.randint(1, numNode * 10)		#adds a tick with a ratio
	packet.append(tick)
	if random.randint(0, 100) <= priorityRatio:		#determine priority through the ratio (random)
		packet.append(1)
	else:
		packet.append(0)
	return packet

# fileName = raw_input('Config File Name: ')
# packetFileName = raw_input('Packet File Name: ')
# topology = input('Pick your topology:\n[1]Star\n[2]Grid\n[3]Mesh\nTopology: ')
# numNode = input('How many nodes: ')
# ratio = input('What is the high-low priorityRatio (in percentage): ')

fileName = sys.argv[1]
packetFileName = sys.argv[2]
topology = int(sys.argv[3])
numNode = int(sys.argv[4])
ratio = int(sys.argv[5])

nodeList = []
nodePackets = {}

for i in range(1, numNode+1):
	nodeList.append(str(i))
	nodePackets[str(i)] = []
	for j in range(0, 5):
		nodePackets[str(i)].append(createPacket(ratio, i))

settingsFile = open(fileName, 'w')
if topology == 1:		#Star topology
	settingsFile.write("Star\n")
	settingsFile.write(str(numNode) +"\n")		#write number of nodes



elif topology == 2:		#Grid topology
	# numCol = input('How many columns: ')
	numCol = int(sys.argv[6])
	settingsFile.write("Grid\n")
	settingsFile.write(str(numNode) +"\n")		#write number of nodes
	settingsFile.write(str(numCol))				#write number of columns

elif topology == 3:		#Mesh topology
	nodeNeighbors = {}
	createNeighbors()	
	settingsFile.write("Mesh\n")
	settingsFile.write(str(numNode) +"\n")		#write number of nodes
	for x in sorted(nodeNeighbors.keys()):
		settingsFile.write(str(x) + ":")		#packet id
		for y in nodeNeighbors[x]:
			settingsFile.write(" " + str(y))	#write related numbers
		settingsFile.write("\n")

settingsFile.close()


def getKey(item):
	return item[2]

packetsFile = open(packetFileName, 'w')	#write packets to file
allPackets = []
for x in nodePackets.keys():	
	for k in nodePackets[x]:
		allPackets.append(k)
sortedPackets = sorted(allPackets, key=getKey)
for k in sortedPackets:
	packetsFile.write(str(k[0]) + " " + "[")	#write source file
	for j in k[1]:
		if j != k[1][-1]:			#if its NOT the last one
			packetsFile.write(str(j) + " ")	#add a space
		else:
			packetsFile.write(str(j) + "]")		#else add the closing bracket
	packetsFile.write(" " + str(k[2]))		#write the tick
	packetsFile.write(" " + str(k[3]) + "\n")	#write the priority
packetsFile.close()