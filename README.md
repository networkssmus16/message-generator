# README #



## NodeList format ##
### Star Topology ###
Star
{Number of Nodes}

### Grid Topology ###
Grid
{Number of Nodes}
{Number of Columns}

### Mesh Topology ###
Mesh
{Number of Nodes}
{Node ID}: [List of neighboring node ids]

======

## PacketList Format ##
{Source} [List of Destinations] {creationTick} {Priority}
